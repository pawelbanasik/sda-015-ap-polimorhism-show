package com.pawelbanasik;


public class Showman {

	// W Javie polimorfizm statyczny to po prostu przeciazanie metod
	
    private static final String MY_NAME = "Pawel";

    public static void main(String[] args) {

        show((double) 4);
        show(4.0);
		Showman showman = new Showman();
        showman.show("HelloWorld");
    }

    private static void show(int i) {

        System.out.println(" liczba typu int : " + i);
    }
    private static void show(double i) {

        System.out.println(" liczba typu double : " + i);
    }
    private void show(String text) {

        System.out.println(" text : " + text);
    }
}
